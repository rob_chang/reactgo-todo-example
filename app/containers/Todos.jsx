
import React, { Component } from 'react';
import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from '../css/components/todo';

const cx = classNames.bind(styles);


class Todos extends Component {
  render() {
    const { items } = this.props;

    const todoItems = items.map((itm) => {
      return (
        <li key={itm._id}>
          <span className={cx('todo')}>{itm.description}</span>
          <span className={cx('todo_status')}>{itm.done ? "(done)" : "(open)"}</span>
        </li>
      );
    });

    return (
      <div>{todoItems}</div>
    );
  }
}

Todos.propTypes = {
  items: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
  return {
    items: state.todo.todos,
  };
}

// Read more about where to place `connect` here:
// https://github.com/rackt/react-redux/issues/75#issuecomment-135436563
export default connect(mapStateToProps, {})(Todos);



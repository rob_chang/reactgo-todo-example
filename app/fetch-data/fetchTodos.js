import { todoService } from '../services';

const fetchTodos = () => {
    console.log("fetch-data:fetchTodos");
    return todoService().getTodos()
        .then(res => res.data)
        // Return empty array in case of errors
        .catch(() => []);
};

export default fetchTodos;
import React, { Component } from 'react';
import Page from '../pages/Page';
import TodosContainer from '../containers/Todos';

class FetchTodos extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle = () => {
    return 'Todos | reactGo';
  };

  pageMeta = () => {
    return [
      { name: 'description', content: 'Test Todo API' }
    ];
  };

  pageLink = () => {
    return [];
  };

  render() {
    return (
      <Page {...this.getMetaData()}>
        <TodosContainer {...this.props} />
      </Page>
    );
  }
}

export default FetchTodos;
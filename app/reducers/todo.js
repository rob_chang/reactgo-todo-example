import { combineReducers } from 'redux';
import * as types from '../types';

const todos = (
  state = [],
  action
) => {

  console.log(">>>STATE:" + action.type + " " + JSON.stringify(state, null, 2));

  var newState = state;

  switch (action.type) {
    case types.REQUEST_SUCCESS:
      console.log(JSON.stringify(action, null, 1));
      if (action.data) {
        if (action.data.todos) {
          newState = action.data.todos;
        }
      }
      break;
    default:
      break;
  }
  return newState;

};

const todoReducer = combineReducers({
  todos
});

export default todoReducer;
import { todoAPIEndpoint } from '../../config/app';
import createRestApiClient from '../utils/createRestApiClient';

export default () => {
  const client = createRestApiClient().withConfig({ baseURL: todoAPIEndpoint });
  return {

    getTodos: () => client.request({
      method: 'GET',
      url: '/api/todo/getall'
    }),    
  };
};